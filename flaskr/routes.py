from flask import Blueprint, render_template, request

from flaskr.models import Driver

race = Blueprint('race', __name__, template_folder='templates')


@race.route('/', methods=['POST', 'GET'])
@race.route('/report/', methods=['POST', 'GET'])
def common_statistic():
    order = request.args.get('order')
    if order == 'asc' or order == None:
        winlist = Driver.select().order_by(Driver.lap.asc())
    else:
        winlist = Driver.select().order_by(Driver.lap.desc())
    return render_template('common_statistic.html', winlist=winlist)


@race.route('/report/drivers/', methods=['POST', 'GET'])
def drivers_list():
    order = request.args.get('order')
    driver_id = request.args.get('driver_id')

    if driver_id is not None:
        winlist = Driver.get(abbr=driver_id)
        driver_request = render_template('driver_info.html', winlist=winlist)
    elif order == 'asc' or order == None:
        winlist = Driver.select().order_by(Driver.name.asc())
        driver_request = render_template('drivers_list.html', winlist=winlist)
    else:
        winlist = Driver.select().order_by(Driver.name.desc())
        driver_request = render_template('drivers_list.html', winlist=winlist)

    return driver_request
