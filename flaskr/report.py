from datetime import datetime, timedelta
import os
import argparse


def read_log(logfile):  # read log files that contain abbreviation, start and end data of the best lap
    dict_log = {}
    with open(logfile, 'r', encoding='UTF-8') as logfile:
        for line in logfile.readlines():
            line = line.strip()
            if len(line) != 0:
                racer_abbr, time_point = [line[:3], datetime.strptime(line[3:26], '%Y-%m-%d_%H:%M:%S.%f')]
                dict_log[racer_abbr] = time_point
            else:
                continue
    return dict_log


def read_abbr(abbrfile):  # read file contains abbreviation explanations
    dict_abbr = {}
    with open(abbrfile, 'r', encoding='UTF-8') as abbrfile:
        for line in abbrfile.readlines():
            line = line.strip()
            if len(line) != 0:
                racer_abbr, racer_name, racer_team = line.split('_')
                dict_abbr[racer_abbr] = {
                    'name': racer_name,
                    'team': racer_team,
                }
            else:
                continue
    return dict_abbr


def build_report(path):  # build dict of dict {driver name:{name:value, team:value, best time of lap:value}}
    racer_abbrs = read_abbr(os.path.join(path, 'abbreviations.txt'))
    lap_start = read_log(os.path.join(path, 'start.log'))
    lap_end = read_log(os.path.join(path, 'end.log'))

    laplist = {}
    for racer, laps in racer_abbrs.items():
        racer_name = racer_abbrs[racer]['name']
        racer_team = racer_abbrs[racer]['team']
        lap = lap_end[racer] - lap_start[racer]

        if lap > timedelta(0):
            lap = str(lap)
        else:
            lap = 'Error time'

        laplist[racer_name] = {
            'name': racer_name,
            'team': racer_team,
            'time': lap,
        }

    return laplist


def sort_report(laplist, reverse=False):  # sort by best time of lap build_report() by asc|desc (default order is asc)
    winlist = dict(sorted(laplist.items(), key=lambda item: item[1]['time'], reverse=reverse))
    return winlist


def print_report(winlist):  # print result of build_report
    for i, drivers in enumerate(winlist.values(), 1):
        name, team, time = drivers.values()
        if i == 16:
            print(f'=' * 69)
        print(f'{i:3}.{name:20} | {team:25} | {time}')


def print_report_driver(laplist, driver):  # print statistic about driver (name, team, best time of lap)
    for drivers in laplist.values():
        name, team, time = drivers.values()
        if name == driver:
            print(f'{name} | {team} | {time}')


def cli_report():  # command-line interface
    parser = argparse.ArgumentParser(description='Report of Monaco 2018 Racing')
    parser.add_argument('-f', '--files', required=True,
                        help='Shows list of drivers and optional order (default order is asc)')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--asc', required=False, dest='asc', action='store_false',
                       help='Optional order is asc)')
    group.add_argument('--desc', required=False, dest='desc', action='store_true',
                       help='Optional order is desc)')
    group.add_argument('--driver', required=False, dest='driver', help='Shows statistic about driver')
    args = parser.parse_args()

    if args.driver:
        print_report_driver(build_report(args.files), args.driver)
    elif args.desc:
        print_report(sort_report(build_report(args.files), reverse=args.desc))
    else:
        print_report(sort_report(build_report(args.files)))


if __name__ == '__main__':
    cli_report()
