from flaskr import create_app

if __name__ == '__main__':
    races = create_app()
    races.run()
