from peewee import *

db = SqliteDatabase(None)


class BaseModel(Model):
    class Meta:
        database = db


class Driver(BaseModel):
    abbr = CharField()
    name = CharField()
    team = CharField()
    start = DateTimeField()
    end = DateTimeField()
    lap = CharField()

    class Meta:
        db_table = 'Driver'
