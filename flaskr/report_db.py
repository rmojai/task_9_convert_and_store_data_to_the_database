import argparse
import os
from datetime import timedelta

# import race_config
from flaskr.models import Driver, db
from flaskr.report import read_log, read_abbr


def racer_data(path_files, path_db):
    db.init(path_db)
    with db:
        db.create_tables([Driver], safe=False)

    racer_abbrs = read_abbr(os.path.join(path_files, 'abbreviations.txt'))
    lap_starts = read_log(os.path.join(path_files, 'start.log'))
    lap_ends = read_log(os.path.join(path_files, 'end.log'))

    for racer, laps in racer_abbrs.items():
        racer_name = racer_abbrs[racer]['name']
        racer_team = racer_abbrs[racer]['team']

        lap_start = lap_starts[racer]
        lap_end = lap_ends[racer]

        lap_time = lap_ends[racer] - lap_starts[racer]
        if lap_time > timedelta(0):
            lap_time = str(lap_time)

        else:
            lap_time = 'Error time'

        Driver.create(
            abbr=racer,
            name=racer_name,
            team=racer_team,
            start=lap_start,
            end=lap_end,
            lap=lap_time
        )


def cli_report():
    parser = argparse.ArgumentParser(description='Report of Monaco 2018 Racing')
    parser.add_argument('-f', '--files', required=True, help='Path to log files')
    parser.add_argument('-d', '--db', required=True, help='Path to database')
    args = parser.parse_args()

    racer_data(args.files, args.db)


if __name__ == '__main__':
    # racer_data(race_config.Config.PATH, race_config.Config.DATABASE)
    cli_report()
