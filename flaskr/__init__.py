from flasgger import Swagger
from flask import Flask

import race_config
from api import api_race
from flaskr.models import db
from flaskr.routes import race


def create_app():
    # create and configure the app
    app = Flask(__name__)
    app.config.from_object('race_config.Config')
    app.register_blueprint(race)
    app.register_blueprint(api_race)
    Swagger(app, template_file='../api/report.yml')

    if app.config['TESTING']:
        database = race_config.Testing.DATABASE
    else:
        database = race_config.Config.DATABASE
    db.init(database)
    return app
