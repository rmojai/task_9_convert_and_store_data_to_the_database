import pytest
from flask import request

from flaskr import create_app


@pytest.fixture()
def races_test():
    races_test = create_app()
    races_test.config.from_object('race_config.Testing')

    yield races_test


@pytest.fixture()
def client(races_test):
    client = races_test.test_client()
    return client


def test_common_statistic(client):
    response = client.get('/report/')
    assert response.status_code == 200


def test_drivers_list(client):
    response = client.get('/report/drivers/')
    assert response.status_code == 200


def test_driver_info(client):
    response = client.get("/report/drivers/?driver_id=BHS")
    assert b"Brendon Hartley" in response.data


def test_request_drivers_desc(races_test):
    with races_test.test_request_context("/report/drivers/?order=desc#"):
        assert request.args['order'] == 'desc'


def test_driver_desc_order(client):
    response = client.get("/report/drivers/?order=desc#")
    assert \
        b'<li>Carlos Sainz</li>\n            </td>\n            <td><a href="/report/drivers/?driver_id=CSR">CSR</a>\n' \
        b'            </td>\n        </tr>\n        \n        <tr>\n            <td>\n                ' \
        b'<li>Brendon Hartley</li>\n            </td>\n            <td><a href="/report/drivers/?driver_id=BHS">BHS</a>\n' \
        in response.data


def test_request_drivers_asc(races_test):
    with races_test.test_request_context("/report/drivers/?order=asc#"):
        assert request.args['order'] == 'asc'


def test_driver_asc_order(client):
    response = client.get("/report/drivers/?order=asc#")
    assert \
        b'<li>Stoffel Vandoorne</li>\n            </td>\n            <td><a href="/report/drivers/?drive' \
        b'r_id=SVM">SVM</a>\n            </td>\n        </tr>\n        \n        <tr>\n            <td>\n                ' \
        b'<li>Valtteri Bottas</li>\n            </td>\n            <td><a href="/report/drivers/?driver_id=VBM">VBM</a>\n' \
        in response.data


def test_request_report_desc(races_test):
    with races_test.test_request_context("/report/?order=desc#"):
        assert request.args['order'] == 'desc'


def test_report_desc_order(client):
    response = client.get("/report/?order=desc#")
    assert \
        b'<li>Valtteri Bottas</li>\n            </td>\n            <td>MERCEDES</td>\n            <td>0:01:12.434000</td>\n' \
        b'\n        </tr>\n        \n        <tr>\n            <td>\n                ' \
        b'<li>Sebastian Vettel</li>\n            </td>\n            <td>FERRARI</td>\n            <td>0:01:04.415000</td>\n' \
        in response.data


def test_request_report_asc(races_test):
    with races_test.test_request_context("/report/?order=asc#"):
        assert request.args['order'] == 'asc'


def test_report_asc_order(client):
    response = client.get("/report/?order=asc#")
    assert \
        b'<li>Esteban Ocon</li>\n            </td>\n            <td>FORCE INDIA MERCEDES</td>\n            <td>Error time</td>\n' \
        b'\n        </tr>\n        \n        <tr>\n            <td>\n                ' \
        b'<li>Sergey Sirotkin</li>\n            </td>\n            <td>WILLIAMS MERCEDES</td>\n            <td>Error time</td>\n' \
        in response.data


if __name__ == '__main__':
    pytest.main()
