import pytest
from unittest.mock import patch, mock_open
import datetime
from flaskr.report import read_log, read_abbr, build_report, sort_report, print_report, print_report_driver


@patch('builtins.open', mock_open(read_data='SVF2018-05-24_12:02:58.917'))
def test_read_log():
    assert read_log('') == {'SVF': datetime.datetime(2018, 5, 24, 12, 2, 58, 917000)}


@patch('builtins.open', mock_open(read_data='DRR_Daniel Ricciardo_RED BULL RACING TAG HEUER'))
def test_read_abbr():
    assert read_abbr('') == {'DRR': {'name': 'Daniel Ricciardo', 'team': 'RED BULL RACING TAG HEUER'}}


@patch('flaskr.report.read_log', side_effect=[{'KMH': datetime.datetime(2018, 5, 24, 12, 2, 51, 3000)},
                                                  {'KMH': datetime.datetime(2018, 5, 24, 12, 4, 4, 396000)}])
@patch('flaskr.report.read_abbr', return_value={'KMH': {'name': 'Kevin Magnussen', 'team': 'HAAS FERRARI'}})
def test_build_report(mock_read_abbr, mock_read_log):
    assert build_report('') == {
        'Kevin Magnussen': {'name': 'Kevin Magnussen', 'team': 'HAAS FERRARI', 'time': '0:01:13.393000'}
    }


def test_sort_report():
    cases = [
        ({'Valtteri Bottas': {'name': 'Valtteri Bottas', 'team': 'MERCEDES', 'time': '0:01:12.434000'},
          'Sebastian Vettel': {'name': 'Sebastian Vettel', 'team': 'FERRARI', 'time': '0:01:04.415000'}},
         {'Sebastian Vettel': {'name': 'Sebastian Vettel', 'team': 'FERRARI', 'time': '0:01:04.415000'},
          'Valtteri Bottas': {'name': 'Valtteri Bottas', 'team': 'MERCEDES', 'time': '0:01:12.434000'}})
    ]
    for laplist, winlist in cases:
        assert sort_report(laplist) == winlist


def test_print_report(capsys):
    cases = [
        ({'Sebastian Vettel': {'name': 'Sebastian Vettel', 'team': 'FERRARI', 'time': '0:01:04.415000'}},
         '  1.Sebastian Vettel     | FERRARI                   | 0:01:04.415000\n')
    ]
    for winlist, print_winlist in cases:
        print_report(winlist)
        captured = capsys.readouterr()
        assert captured.out == print_winlist


def test_print_report_driver(capsys):
    cases = [
        ({'Sebastian Vettel': {'name': 'Sebastian Vettel', 'team': 'FERRARI', 'time': '0:01:04.415000'}},
         'Sebastian Vettel',
         'Sebastian Vettel | FERRARI | 0:01:04.415000\n')
    ]
    for laplist, driver, print_stat in cases:
        print_report_driver(laplist, 'Sebastian Vettel')
        captured = capsys.readouterr()
        assert captured.out == print_stat


if __name__ == '__main__':
    pytest.main()
