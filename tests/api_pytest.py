import pytest
import requests
from flask import request, json

from flaskr import create_app

baseUrl = "http://127.0.0.1:5000/api/v1"


@pytest.fixture()
def races_test():
    races_test = create_app()
    races_test.config.from_object('race_config.Testing')

    yield races_test


@pytest.fixture()
def client(races_test):
    client = races_test.test_client()
    return client


def test_report():
    path = "/report/"
    response = requests.get(baseUrl + path)
    assert response.status_code == 200


def test_report_order_format():
    path = "/report/?order=asc&format=json"
    response = requests.get(baseUrl + path)
    assert response.status_code == 200


def test_request_report_asc(races_test):
    path = "/report/?order=asc"
    with races_test.test_request_context(baseUrl + path):
        assert request.args['order'] == 'asc'


def test_driver_asc_order(client):
    path = "/report/?order=asc"
    response = client.get(baseUrl + path)
    assert \
        b'"1": {\n    "name": "Sebastian Vettel",\n    "team": "FERRARI",\n    "time": "0:01:04.415000"\n  },\n  ' \
        b'"2": {\n    "name": "Valtteri Bottas",\n    "team": "MERCEDES",\n    "time": "0:01:12.434000"' \
        in response.data


def test_request_report_desc(races_test):
    path = "/report/?order=desc"
    with races_test.test_request_context(baseUrl + path):
        assert request.args['order'] == 'desc'


def test_driver_desc_order(client):
    path = "/report/?order=desc"
    response = client.get(baseUrl + path)
    assert \
        b'"18": {\n    "name": "Valtteri Bottas",\n    "team": "MERCEDES",\n    "time": "0:01:12.434000"\n  },\n  ' \
        b'"19": {\n    "name": "Sebastian Vettel",\n    "team": "FERRARI",\n    "time": "0:01:04.415000"' \
        in response.data


def test_request_report_json(races_test):
    path = "/report/?format=json"
    with races_test.test_request_context(baseUrl + path):
        assert request.args['format'] == 'json'


def test_driver_json_format(client):
    path = "/report/?format=json"
    response = client.get(baseUrl + path)
    assert response.mimetype == 'application/json'


def test_driver_json_format2(client):
    path = "/report/?format=json"
    response = client.get(baseUrl + path)
    assert response.is_json == True


def test_driver_json_format3(client):
    path = "/report/?format=json"
    response = client.get(baseUrl + path)
    data = json.loads(response.get_data(as_text=True))
    assert data['1'] == {"name": "Sebastian Vettel", "team": "FERRARI", "time": "0:01:04.415000"}


def test_request_report_xml(races_test):
    path = "/report/?format=xml"
    with races_test.test_request_context(baseUrl + path):
        assert request.args['format'] == 'xml'


def test_driver_xml_format(client):
    path = "/report/?format=xml"
    response = client.get(baseUrl + path)
    assert response.mimetype == 'application/xml'


if __name__ == '__main__':
    pytest.main()
