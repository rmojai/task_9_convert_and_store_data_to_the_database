from flask_restful import Api

from api.api_resource import api_race, Report

api = Api(api_race)

api.add_resource(Report, '/report/', endpoint='report')
