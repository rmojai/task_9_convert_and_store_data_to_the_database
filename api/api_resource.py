from dict2xml import dict2xml
from flask import Response, Blueprint, jsonify, request
from flask_restful import Resource

from flaskr.models import Driver

api_race = Blueprint('api_race', __name__, url_prefix='/api/v1')


class Report(Resource):
    def get(self):
        order = request.args.get('order')
        format = request.args.get('format')

        if order == 'asc' or order == None:
            winlist = Driver.select().order_by(Driver.lap.asc())
        else:
            winlist = Driver.select().order_by(Driver.lap.desc())

        report = {}
        for drivers in winlist:

            driver_name = drivers.name
            driver_team = drivers.team
            driver_time = drivers.lap

            num = 1
            for i in report:
                num += 1

            report[num] = {
                'name': driver_name,
                'team': driver_team,
                'time': driver_time,
            }

        if format == 'json' or format == None:
            return jsonify(report)
        elif format == 'xml':
            report_xml = dict2xml(report)
            return Response(report_xml, mimetype='application/xml')
