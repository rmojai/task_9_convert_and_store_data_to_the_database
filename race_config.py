class Config(object):
    SECRET_KEY = 'dev'
    DEBUG = True
    PATH = r'..\instance\logs'
    DATABASE = r'..\instance\report.db'


class Testing(Config):
    TESTING = True
    DATABASE = ':memory:'
